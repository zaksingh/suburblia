class StaticPagesController < ApplicationController
  def home
    respond_to do |format|
      format.html
      format.js
    end
  end

  def help
  end
  
  def about
  end
  
  def contact
  end
  
  def feed
    if user_signed_in?
      @feed_items = current_user.feed
    end
  end
end
