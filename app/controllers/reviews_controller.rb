class ReviewsController < ApplicationController
  before_action :set_restaurant #find rest b4 any action
  before_action :authenticate_user!
  
  def new
    #Find correct restaurant and plug it in
    @review = Review.new(restaurant: @restaurant)
  end
  
  def create
    # fill user ID with user
    @review = current_user.reviews.build(review_params)
    @review.restaurant = @restaurant
    @review.save
    redirect_to @restaurant
  end
  
  private
    def set_restaurant
      @restaurant = Restaurant.find(params[:restaurant_id])
    end

    def review_params
      params.require(:review).permit(:comment, :rating)
    end
end