class RelationshipsController < ApplicationController
  before_action :user_signed_in?
  
  def create
    @restaurant = Restaurant.find(params[:followed_id])
    current_user.follow(@restaurant)
    respond_to do |format|
      format.html { redirect_to @restaurant }
      format.js 
    end    
  end
  
  def destroy
    @restaurant = Relationship.find(params[:id]).followed
    current_user.unfollow(@restaurant)
    respond_to do |format|
      format.html { redirect_to @restaurant }
      format.js
    end
  end
end
