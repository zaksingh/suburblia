class RestaurantsController < ApplicationController
  include RestaurantHelper
  #Make sure user is logged in for important pages
  before_action :set_user, except: :create
  before_action :authenticate_user!, only: [:create, :new, :edit, :update, :destroy]
  def index
    #visitor_latitude = request.location.latitude
    #visitor_longitude = request.location.longitude
    visitor_latitude = 37.8858
    visitor_longitude = -122.1180
    
    #use geocoder to filter restaurants by proximity
    #@restaurants = Restaurant.near([visitor_latitude, visitor_longitude], 30)
    
    @filterrific = initialize_filterrific(
            Restaurant,
            params[:filterrific],
            select_options: {
              sorted_by: Restaurant.options_for_sorted_by,
              with_category_id: Category.options_for_select
            },
            persistence_id: false,
            default_filter_params: {},
            available_filters: [:sorted_by, :search_query, :with_category_id],
          ) or return      
          
    
    @restaurants = Restaurant.paginate(:page => params[:page])
    
    @hash = Gmaps4rails.build_markers(@restaurants) do |restaurant, marker|
      marker.lat restaurant.latitude
      marker.lng restaurant.longitude
      marker.infowindow restaurant.name
    end
        
    #Respond with html for page load and js for AJAX updates
    respond_to do |format|
      format.html
      format.js
    end  
    
    #this declaration at end so user sees these results
    @restaurants = @filterrific.find.page(params[:page])
    
    rescue ActiveRecord::RecordNotFound => e
      # There is an issue with the persisted param_set. Reset it.
      puts "Had to reset filterrific params: #{ e.message }"
      redirect_to(reset_filterrific_url(format: :html)) and return

  end

  def new
    @restaurant = Restaurant.new(user: @user) #create a new restaurant
  end
  
  def create
    # fill user ID with user
    @restaurant = current_user.restaurants.build(restaurant_params)
    @restaurant.save
    redirect_to @restaurant
  end
  
  def show
    @restaurant = Restaurant.find(params[:id])
    @microposts = Micropost.where(restaurant_id: @restaurant)
    @reviews = Review.where(restaurant_id: @restaurant)
    if @reviews.blank?
      @avg_rating = 0
    else
      #if there are reviews, average them
      @avg_rating = @reviews.average(:rating).round(2)
    end
    
    @hash = Gmaps4rails.build_markers(@restaurant) do |restaurant, marker|
      marker.lat restaurant.latitude
      marker.lng restaurant.longitude
      marker.infowindow restaurant.description
    end
    respond_to do |format|
      format.html
      format.js
    end      
  end
  
  def edit
    @restaurant = Restaurant.find(params[:id])
  end
  
  def update
    @restaurant = Restaurant.find(params[:id])
    @restaurant.update(restaurant_params)
    redirect_to @restaurant
  end
  
  def destroy
    @restaurant = Restaurant.find(params[:id])
    @restaurant.destroy
    redirect_to root_path
  end
  
  def followers
    @title = "Followers"
    @restaurant  = Restaurant.find(params[:id])
    @users = @restaurant.followers.paginate(page: params[:page])
    render 'show_follow'
  end  
  
  private # remember to add future params here
    def set_user
      @user = current_user
    end  
    def restaurant_params
      params.require(:restaurant).permit(:name, :description, :category_id,
      :address1, :address2, :city, :state, :zipcode, :phone, :email, :image, :user_id)
    end
    
    # thwart hackers
    def is_admin?
      if !current_user.id == @restaurant.user_id
        flash[:danger] = "You are not authorized to edit or delete"
        redirect_to root_path
      end
    end
end  