class MicropostsController < ApplicationController
  before_action :set_restaurant #find rest b4 any action
  before_action :authenticate_user!
  def new
    @micropost = Micropost.new(restaurant: @restaurant)
  end  
  def create
    @micropost = current_user.microposts.build(micropost_params)
    @micropost.restaurant = @restaurant
    if @micropost.save
      flash[:success] = "Micropost created!"
    else
      @feed_items = []
      render 'static_pages/feed'
    end
    redirect_to @restaurant
  end

  def destroy
  end
  
  private
    def set_restaurant
      @restaurant = Restaurant.find(params[:restaurant_id])
    end
    
    def micropost_params
      params.require(:micropost).permit(:content)
    end
end
