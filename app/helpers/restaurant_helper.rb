module RestaurantHelper
  def check_if_owner?(restaurant)
    return false unless current_user
    @restaurant.user_id == current_user.id
  end
end