class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  has_many :reviews
  has_many :restaurants
  has_many :microposts, dependent: :destroy
  #A User is always the "follower", never the "followed".
  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy
     
  has_many :following, through: :active_relationships, source: :followed

  def feed
    following_ids = "SELECT followed_id FROM relationships
                     WHERE  follower_id = :user_id"
    Micropost.where("restaurant_id IN (#{following_ids})", user_id: id)
  end
  
  def follow(restaurant)
    active_relationships.create(followed_id: restaurant.id)
  end
  
  def unfollow(restaurant)
    following.delete(restaurant)
  end  
  
  def following?(restaurant)
    following.include?(restaurant)
  end      
end
