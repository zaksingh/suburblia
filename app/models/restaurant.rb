class Restaurant < ActiveRecord::Base
  
    
  filterrific(
    default_filter_params: { sorted_by: 'name_desc' },
    available_filters: [
      :sorted_by,
      :search_query,
      :with_category_id
    ]
  )
  
  has_many :reviews
  has_many :microposts
  #Restaurant is always the "followed", never the "follower".
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy    
                                   
  has_many :followers, through: :passive_relationships, source: :follower
  
  belongs_to :category
  belongs_to :user
  validates_presence_of :name, :city, :state
  
  geocoded_by :full_address
  after_validation :geocode
  
  mount_uploader :image, ImageUploader #for carrierwave
  
  self.per_page = 12 #for pagination
  
  def full_address
    #return full address for google maps
    [address1, address2, city, state, zipcode].join(',')
  end

  scope :search_query, lambda { |query|
    return nil  if query.blank?
  
    # condition query, parse into individual keywords
    terms = query.downcase.split(/\s+/)
  
    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      ('%'+e.gsub('*', '%') + '%').gsub(/%+/, '%')
    }
    # configure number of OR conditions for provision
    # of interpolation arguments. Adjust this if you
    # change the number of OR conditions.
    num_or_conds = 1
    where(
      terms.map { |term|
        "(LOWER(restaurants.name) LIKE ?)"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conds }.flatten
    )
  }
scope :with_category_id, lambda { |category_ids|
  where(category_id: [*category_ids])
}

scope :sorted_by, lambda { |sort_option|
  # extract the sort direction from the param value.
  direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
  case sort_option.to_s
  when /^name_/
    order("LOWER(restaurants.name) #{ direction }")
  else
    raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
  end 
}

  def self.options_for_sorted_by
    [
      ['Name (a-z)', 'name_asc']
    ]
  end
  
end