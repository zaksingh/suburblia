class Review < ActiveRecord::Base
  
  belongs_to :restaurant
  belongs_to :user
  
  #prevent user from leaving multiple reviews on same business
  validates :user_id, :uniqueness => {:scope => :restaurant_id} 
  
end