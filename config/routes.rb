Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  resources :users, :shallow => true do
    resources :restaurants do
      member do
        get :following, :followers
      end
      resources :reviews, except: [:index, :show] #create review paths
      resources :microposts, only: [:create, :destroy]
    end
  end
  resources :relationships, only: [:create, :destroy]
  root 'static_pages#home'
  get  '/restaurants',  to: 'restaurants#index'
  get  '/about',        to: 'static_pages#about'
  get  '/contact',      to: 'static_pages#contact'
  get  '/feed',      to: 'static_pages#feed'
  
end
