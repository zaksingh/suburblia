class AddRestaurantIndexToMicropost < ActiveRecord::Migration[5.0]
  def change
    add_reference :microposts, :restaurant, index: true #tie to restaurant
  end
end
