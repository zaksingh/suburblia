class AddUserRestaurantIndexToReviews < ActiveRecord::Migration[5.0]
  def change
    add_reference :reviews, :restaurant, index: true #tie to restaurant
    add_reference :reviews, :user, index: true #tie to user
  end
end
