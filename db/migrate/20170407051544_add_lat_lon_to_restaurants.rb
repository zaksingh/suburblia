class AddLatLonToRestaurants < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurants, :latitude, :float #floats for geocoder
    add_column :restaurants, :longitude, :float
  end
end
