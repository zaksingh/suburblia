class AddAdminToUsers < ActiveRecord::Migration[5.0]
  def change
    #boolean admin status
    add_column :users, :admin, :boolean, default: false
  end
end
