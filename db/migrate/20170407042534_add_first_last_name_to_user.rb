class AddFirstLastNameToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :first_name, :string #add first name
    add_column :users, :last_name, :string #add last name
  end
end
