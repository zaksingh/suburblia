class RemoveHoursFromRestaurants < ActiveRecord::Migration[5.0]
  def change
    remove_column :restaurants, :hours, :text, {}
  end
end
